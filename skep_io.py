from skeputils import *
def read_data(filename):
	f = open(filename)
	data = f.read().split()
	npairs = int(data.pop(0))
	nalt = int(data.pop(0))
	narcs = int(data.pop(0))
	arcs = set()
	w = {}
	r = {}
	P = set()
	N = set()
	print("P ", npairs, "N ", nalt, "A", narcs)
	for a in range(narcs):
		i = int(data.pop(0))
		j = int(data.pop(0))
		assert i >= 0 and j >= 0
		if (i < npairs):
			P.add(i)
		else:
			N.add(i)
		if (j < npairs):
			P.add(j)
		else:
			print("incoming arc to altruist %d, %d! exiting" % (i, j))
			exit(1)
		arcs.add((i, j))
		w[i, j] = float(data.pop(0))
		r[i, j] = int(data.pop(0))
	if (len(P) != npairs):
		print("len P != npairs", len(P), npairs)
		exit(1)
	if (len(N) != nalt):
		print("len N ! = ndds ", len(N), nalt)
		exit(1)
	return P, N, arcs, w, r

def read_donors_ranks(filename, nv):
	f = open(filename)
	data = f.read().split()
	r={}
	for v in range(nv):
		i = int(data.pop(0))
		rank = int(data.pop(0))
		r[i] = rank
	return r

