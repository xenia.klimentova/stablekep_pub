# StableKEP_pub

Implementation of Integer Programming models for solving (strongly) stable kidney exchange problem.

## Files

`enum_cycles.py` - functions for enumeration of cycles and chains in a graph

`skep_io.py` - reading data from input files functions

`skeputils.py` - some utils for creation of data structures

`stable_modelling.py` - functions for creation of IP models constraints and variables


`run_models.py` - main model to be run, creates and optimize models

## Folders

`instances_all` - should contain input files with instances with strict preferences; some input files are provided in the project; more instances are available at [Repository](https://doi.org/10.25747/xh4y-2r05) 

`instances_nonstrict` - instances with weak preferences (with ties)

## To run

The sizes and codes of test instances to be tested, the models to be run should be defined in the code of module  `run_models.py` (see lines 501-522):

- `Sizes` - list of sizes of instances

- `instancesset` - list of indices of instances, i-1 from the index in the name of instance *size_i.input*, i.e. if you solve instance *20_1.input*, then 
```
Sizes = [20] 
instancesset = [0]
```

- `modcodes` - list of codes of formulations to be run

Examples of call:
```
python3 run_models.py 3 3 outfile.txt 0 S
python3 run_models.py 4 4 outfile.txt 0 S 0.0

```
where arguments of `run_models.py` module are: 

- K - max length of cycles

- L - max length of chains

- outputfile  - name of output file

- stability - code for stability type to be considered: 0 for stability, 1 for strong stability 

- preferences - types of instances to be considered: S for strict preferences and  N for weak preferences(with ties).

- kappa - optional parameter for analysis of the trade-off between functions; when used formulations with codes 9_ should be used.


### Formulations' codes


- 0  - cycle formulation

- 2  - edge formulation

- 4  - CEF x-objective


- 6  - CEF y-objective

- 8  - piCEF x,z-objective

- 10 - piCEF y-objective

- 1,3,5,7,9,11 - corresponding formulations for finding strongly stable exchange


For analysis of trade-off for two objective functions:

- 9_ - formulations for relaxing of stability (with slack variables). Exists 90,91,94,95


888 - maximum number of transplants

999 - maximize number of transplants for trade-off analysis.




222- maximum number of transplants with K,L  unbounded

20,30,33 - stable, strongly stable, competitive allocations for unbounded case (house market problem)

