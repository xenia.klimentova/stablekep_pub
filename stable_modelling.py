import gurobipy as grb
from enum_cycles import *


class modelling(object):
    def __init__(self):
        self.V = []
        self.P = []
        self.N = []

        self.C = []
        self.aincyc = {}
        self.vincyc = {}

        self.ainc = {}
        self.vinc = {}
        self.r = {}

        self.arcs = {}
        self.adj_out = {}
        self.adj_in = {}

        self.arcs_dummy = {}
        self.adj_out_dummy = {}
        self.adj_in_dummy = {}

    # ================================================================
    #  ======== creation of variables ===============================
    # ================================================================

    def add_x_vars(self, model, coefs, type, ccs=None):
        x = []

        # create variables for all set C of cycles and chains if no set of cycles provided, or for a given set ccs
        if ccs != None:
            CC = ccs
        else:
            CC = self.C

        i = 0
        for c in CC:
            x.append(model.addVar(obj=coefs[i], vtype=type, name="x%d" % (i,),
                                  lb=0.0, ub=1.0))
            i += 1
        model.update()
        return x

    def add_slack_vars(self, model, coefs, type):
        tau = []
        i = 0
        for c in self.C:
            tau.append(model.addVar(obj=coefs[i], vtype=type, name="tau%d" % (i,), lb=0.0))
            i += 1
        model.update()
        return tau

    def add_y_vars(self, model, coefs, type):
        y = {}
        for a in self.arcs_dummy:
            y[a] = model.addVar(obj=coefs[a], vtype=type, name="y%d,%d" % (a[0], a[1],), lb=0.0, ub=1.0)
            if (len(self.ainc[a]) == 0):
                y[a].ub = 0.0
        model.update()
        return y

    def add_z_vars(self, model, coefs, calK_l, graph_l_adj_out, type):
        z = {}

        for l in graph_l_adj_out:
            for i in graph_l_adj_out[l]:
                for j in graph_l_adj_out[l][i]:
                    for k in calK_l[l][(i, j)]:
                        ind = (l, i, j, k)
                        z[ind] = model.addVar(obj=coefs[(i, j)], name="s%d,%d,%d,%d" % (ind[0], ind[1], ind[2], ind[3]),
                                              vtype=type, lb=0.0, ub=1.0)

        model.update()
        return z

    def add_y_vars_unbounded(self, model, coefs, type):
        y = {}
        for a in self.arcs_dummy:
            # y[a] = model.addVar(obj=1.0,  name="y%d,%d" % (a[0], a[1],), lb=0.0, ub=1.0)
            y[a] = model.addVar(obj=coefs[a], vtype=type, name="y%d,%d" % (a[0], a[1],), lb=0.0, ub=1.0)
        model.update()
        return y

    def add_p_vars_unbounded(self, model, type):
        p = []
        for v in self.V:
            p.append(model.addVar(obj=0.0, vtype=type, name="p%d" % (v), lb=1.0, ub=len(self.V)))
        model.update()
        return p

    def add_u_vars(self, model, coefs, type):
        u = {}
        for a in self.arcs_dummy:
            # y[a] = model.addVar(obj=1.0,  name="y%d,%d" % (a[0], a[1],), lb=0.0, ub=1.0)
            u[a] = model.addVar(obj=coefs[a], vtype=type, name="u%d,%d" % (a[0], a[1],), lb=0.0, ub=1.0)
            if (len(self.ainc[a]) == 0):
                u[a].ub = 0.0
        model.update()
        return u

    # ================================================================
    # =========== constraints, cycle formulation  =======================
    # ================================================================

    def add_packing(self, model, x):

        for v in self.V:
            coef = []
            vari = []
            for ic in self.vinc[v]:
                coef.append(1)
                vari.append(x[ic])

            if len(coef) > 0:
                expr = grb.LinExpr(coef, vari)
                model.addConstr(lhs=expr, sense=grb.GRB.LESS_EQUAL, rhs=1.0, name="pack_%d" % (v,))

    def add_cycle_stability(self, model, x, tau=None):
        b_then_c = []
        for c in self.C:
            i_c = self.C.index(c)
            b_then_c.append(set())

            b_then_c[i_c].add(i_c)
            for ivc in range(len(c)):
                for i_s in self.vinc[c[ivc]]:
                    if (i_s == i_c):
                        continue

                    ivs = self.C[i_s].index(c[ivc])
                    arc_c = (c[ivc - 1], c[ivc])
                    arc_s = (self.C[i_s][ivs - 1], self.C[i_s][ivs])
                    # print(ivs,",", arc_c, arc_s)
                    # print(r)
                    if (self.r[arc_c] >= self.r[arc_s]):
                        b_then_c[i_c].add(i_s)
        if (tau == None):
            model.addConstrs(
                (grb.quicksum(x[i] for i in b_then_c[ic]) >= 1.0 for ic in range(len(self.C))))  # , name ="stab_")
        else:
            model.addConstrs(
                (grb.quicksum(x[i] for i in b_then_c[ic]) + tau[ic] >= 1.0 for ic in
                 range(len(self.C))))  # , name ="stab_")

    def add_cycle_strong_stability(self, model, x, tau=None):
        b_then_c = []
        eq_c = []
        for c in self.C:
            b_then_c.append(set())
            eq_c.append([])
            i_c = self.C.index(c)
            b_then_c[i_c].add(i_c)
            for ivc in range(len(c)):
                for i_s in self.vinc[c[ivc]]:
                    if (i_s == i_c):
                        continue

                    ivs = self.C[i_s].index(c[ivc])
                    arc_c = (c[ivc - 1], c[ivc])
                    arc_s = (self.C[i_s][ivs - 1], self.C[i_s][ivs])
                    # print(ivs,",", arc_c, arc_s)
                    # print(r)
                    if (self.r[arc_c] > self.r[arc_s]):
                        b_then_c[i_c].add(i_s)
                    if (self.r[arc_c] == self.r[arc_s]):
                        eq_c[i_c].append(i_s)
            if (len(eq_c[i_c]) < len(c)):
                eq_c[i_c] = []

        if (tau == None):
            model.addConstrs((grb.quicksum(x[i] for i in b_then_c[ic]) +
                              grb.quicksum(1.0 / len(self.C[ic]) * x[i] for i in eq_c[ic]) >= 1.0
                              for ic in range(len(self.C))))  # , name="sstab")
        else:
            model.addConstrs((grb.quicksum(x[i] for i in b_then_c[ic]) +
                              grb.quicksum(1.0 / len(self.C[ic]) * x[i] for i in eq_c[ic]) + tau[ic] >= 1.0
                              for ic in range(len(self.C))))  # , name="sstab")

    # ===========================================================================================================
    # ====================== constraints for edge and cycle-edge formulation =====================================
    # ===========================================================================================================
    def add_flowconservation(self, model, y):
        for i in self.V:
            coef = []
            vari = []
            for j in self.adj_in_dummy[i]:
                coef.append(1)
                vari.append(y[j, i])
            for j in self.adj_out_dummy[i]:
                coef.append(-1)
                vari.append(y[i, j])
            if len(coef) > 0:
                expr = grb.LinExpr(coef, vari)
                model.addConstr(lhs=expr, sense=grb.GRB.EQUAL, rhs=0.0, name="flow_%d" % (i,))

    def add_packing_edge(self, model, y):
        for i in self.V:
            coef = []
            vari = []
            for j in self.adj_out_dummy[i]:
                coef.append(1)
                vari.append(y[i, j])
            if len(coef) > 0:
                expr = grb.LinExpr(coef, vari)
                model.addConstr(lhs=expr, sense=grb.GRB.LESS_EQUAL, rhs=1.0, name="packingE_%d" % (i,))

    def add_connection(self, model, x, y):

        # sum (ij)in c x_c= y_ij
        model.addConstrs(grb.quicksum(x[ic] for ic in self.ainc[a]) == y[a] for a in self.arcs_dummy)  # , name ="xa_y")

    def add_edge_stability(self, model, y, tau=None, ccs=None):

        if ccs == None:
            CC = self.C
        else:
            CC = ccs

        b_arcs = []

        for c in CC:
            ic = CC.index(c)
            b_arcs.append(set())
            for i in range(len(c)):
                b_arcs[ic].add((c[i - 1], c[i]))
                for v in self.adj_in_dummy[c[i]]:
                    if self.ainc[v, c[i]] == []:
                        continue
                    if self.r[v, c[i]] <= self.r[c[i - 1], c[i]]:
                        b_arcs[ic].add((v, c[i]))
        if (tau == None):
            model.addConstrs(grb.quicksum(y[a] for a in b_arcs[ic]) >= 1.0
                             for ic in range(len(CC)))  # , name = "stabE")
        else:
            model.addConstrs(grb.quicksum(y[a] for a in b_arcs[ic]) + tau[ic] >= 1.0
                             for ic in range(len(CC)))  # , name = "stabE")

    def add_edge_strong_stability(self, model, y, tau=None):
        b_arcs = []
        eq_arcs = []
        for c in self.C:
            ic = self.C.index(c)
            coef = []
            vari = []
            b_arcs.append(set())
            eq_arcs.append(set())
            for i in range(len(c)):
                eq_arcs[ic].add((c[i - 1], c[i]))
                for v in self.adj_in_dummy[c[i]]:
                    if (self.ainc[v, c[i]] == []):
                        continue
                    if (self.r[v, c[i]] < self.r[c[i - 1], c[i]]):
                        b_arcs[ic].add((v, c[i]))
                    if (self.r[v, c[i]] == self.r[c[i - 1], c[i]] and v != c[i - 1]):
                        eq_arcs[ic].add((v, c[i]))
        if (tau == None):
            model.addConstrs(grb.quicksum(len(self.C[ic]) * y[a] for a in b_arcs[ic]) +
                             grb.quicksum(y[a] for a in eq_arcs[ic]) >= len(self.C[ic])
                             for ic in range(len(self.C)))  # , name = "sstabE")
        else:
            model.addConstrs(grb.quicksum(len(self.C[ic]) * y[a] for a in b_arcs[ic]) +
                             grb.quicksum(y[a] for a in eq_arcs[ic]) + len(self.C[ic]) * tau[ic] >= len(self.C[ic])
                             for ic in range(len(self.C)))  # , name = "sstabE")

    # ===========================================================================================================
    # =========== constraints for PICEF =========================================================================
    # ===========================================================================================================

    def add_picef_packing(self, model, x, z, graphs_l_adj_out, cal_K):
        for i in self.P:
            coef = []
            vari = []
            for ic in self.vincyc[i]:
                coef.append(1)
                vari.append(x[ic])
            for l in graphs_l_adj_out:
                if i in graphs_l_adj_out[l]:
                    for j in graphs_l_adj_out[l][i]:
                        for k in cal_K[l][(i, j)]:
                            coef.append(1)
                            vari.append(z[l, i, j, k])

            if len(coef) > 0:
                expr = grb.LinExpr(coef, vari)
                model.addConstr(lhs=expr, sense=grb.GRB.LESS_EQUAL, rhs=1.0, name="pack_%d" % (i,))

    def add_nddpicef_packing(self, model, z, graphs_l_adj_out):
        model.addConstrs((grb.quicksum(z[(n, n, j, 1)] for j in self.adj_out[n]) <= 1.0 for n in self.N))

    def add_picef_positioning(self, model, z, graphs_l_adj_in, graphs_l_adj_out, calK, L):
        model.addConstrs((grb.quicksum(z[(l, j, i, k)] for j in graphs_l_adj_in[l][i] if k in calK[l][(j, i)]) -
                          grb.quicksum(z[(l, i, j, k + 1)]
                                       for j in graphs_l_adj_out[l][i] if k + 1 in calK[l][(i, j)]) == 0
                          for l in graphs_l_adj_out for i in graphs_l_adj_out[l] if i != l for k in range(1, L)))

    def add_picef_connection(self, model, x, y, z, graph_l_adj_out, calK_l):
        for a in self.arcs_dummy:
            coef = []
            vari = []
            for l in graph_l_adj_out:
                if a[0] in graph_l_adj_out[l]:
                    if a[1] in graph_l_adj_out[l][a[0]]:
                        for k in calK_l[l][a]:
                            coef.append(1)
                            vari.append(z[(l, a[0], a[1], k)])
            if a in self.aincyc:
                for ic in self.aincyc[a]:
                    coef.append(1)
                    vari.append(x[ic])
            coef.append(-1)
            vari.append(y[a])
            if len(coef) > 0:
                expr = grb.LinExpr(coef, vari)
                model.addConstr(lhs=expr, sense=grb.GRB.EQUAL, rhs=0.0, name="connect_%d,%d" % (a[0], a[1]))
        # model.addConstrs((grb.quicksum(z[(l, a[0], a[1], k)] for l in graph_adj_out_l if a[0] in graph_adj_out_l[l]
        #                                 if a[1] in graph_adj_out_l[l][a[0]] for k in calK_l[a]) +
        #                   grb.quicksum(x[ic] for ic in self.aincyc[a]) - y[a] == 0 for a in self.arcs_dummy))

    # ===========================================================================================================
    # =========== constraints for unbounded edge formulation ====================================================
    # ===========================================================================================================

    def add_edge_stability_unbounded(self, model, y, p):

        model.addConstrs(len(self.V) * grb.quicksum(y[k, a[1]] for k in self.adj_in_dummy[a[1]]
                                                    if self.r[k, a[1]] <= self.r[a[0], a[1]]) + p[a[0]] - p[a[1]] >= 1
                         for a in self.arcs_dummy)

    def add_edge_competitive_unbounded(self, model, y, p):
        model.addConstrs(len(self.V) * (1 - y[i, j]) + p[i] - p[j] >= 0 for (i, j) in self.arcs_dummy)

    def add_edge_strong_stability_unbounded(self, model, y, p):

        model.addConstrs(len(self.V) * (y[a[0], a[1]] + grb.quicksum(y[k, a[1]] for k in self.adj_in_dummy[a[1]]
                                                                     if self.r[k, a[1]] < self.r[a[0], a[1]])) + p[
                             a[0]] - p[a[1]] >= 1 for a in self.arcs_dummy)

    # ===========================================================================================================
    ## =========== constraints for alternative edge formulation ====================
    # ===========================================================================================================
    def add_u_stability(self, model, y, u):

        model.addConstrs((u[i, j] + grb.quicksum(
            y[k, j] for k in self.adj_in_dummy[j] if (self.r[k, j] <= self.r[i, j] and self.ainc[k, j] != [])) == 1
                          for (i, j) in self.arcs_dummy if len(self.ainc[i, j]) > 0))  # , name = "ustab")

    def add_u_strong_stability(self, model, y, u):

        model.addConstrs(
            (u[i, j] + grb.quicksum(y[k, j] for k in self.adj_in_dummy[j] if self.r[k, j] < self.r[i, j]) == 1
             for (i, j) in self.arcs_dummy if len(self.ainc[i, j]) > 0), name="ustab")

    def add_u_cycle_card(self, model, u):
        model.addConstrs(
            (grb.quicksum(u[self.C[ic][i - 1], self.C[ic][i]] for i in range(len(self.C[ic]))) <= len(self.C[ic]) - 1
             for ic in range(len(self.C))), name="u_card")

    # ===========================================================================================================
    # =========== constraints for relaxing stability ============================================================
    # ===========================================================================================================

    def add_relax_bound(self, model, coefs, vars, rhs):
        model.addConstr(grb.quicksum(coefs[i] * vars[i] for i in range(len(vars)))
                        >= rhs, name="RELAXEDMAX")
