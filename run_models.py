import gurobipy as grb
from skep_io import *
from enum_cycles import *
import time
from time import process_time
from stable_modelling import *

# output files
fileoutres = "Stab_noflow.out"

# some paremeters
numthreads = 1
maxtime = 3600
# maxtime = 12
mymipgap = 0

numCycles = 0
numChains = 0
numPaths = 0

rhsdelta = -1
ObjMaxCF = -1


def prepare(model):
    model.setParam("LogToConsole", 0)
    model.setParam("Threads", numthreads)
    model.setParam("MIPGap", mymipgap)


# model.setParam("Presolve", 0)


def add_paths(model, y):
    stt = process_time()
    paths = get_all_chains(P, adj_out, K + 1, checker=lambda i: len(i) == K + 1)
    # print(paths)
    paths_a = get_all_chains(N, adj_out, L + 1, checker=lambda i: len(i) == L + 1)
    # print(paths_a)

    np = len(paths) + len(paths_a)
    print("Find paths: ", process_time() - stt)
    print("number of paths found", np)
    stt = process_time()

    model.addConstrs(
        grb.quicksum(y[p[i], p[i + 1]] for i in range(len(p) - 1)) <= K - 1 for p in paths)  # , name = "paths")
    model.addConstrs(
        grb.quicksum(y[p[i], p[i + 1]] for i in range(len(p) - 1)) <= K - 1 for p in paths_a)  # , name = "paths_a")

    print("Add path constraints: ", process_time() - stt)

    return np


def model(mod, newline, endline):
    strt = process_time()

    model = grb.Model()
    prepare(model)
    model.ModelSense = -1
    # =========== to find maximal nubmer of transplants
    if (mod == 888 or mod == 999):
        coefsx = [len(c) for c in C]
        x = m.add_x_vars(model, coefsx, grb.GRB.BINARY)
        m.add_packing(model, x)
        lpfile = "cf.lp"

    ## ================================================================================================
    ## ====== cycle formulations ======================================================================
    ## ====== 9 - cycle formulations for relaxation of stability and price of stability ===============
    ## ================================================================================================

    if (mod == 0 or mod == 1 or mod == 90 or mod == 91):
        # coefs = [sum(w[c[i - 1], c[i]] for i in range(len(c))) for c in C]
        coefsx = [len(c) for c in C]
        x = m.add_x_vars(model, coefsx, grb.GRB.BINARY)
        m.add_packing(model, x)

        if (mod < 10):
            if (mod == 0):
                m.add_cycle_stability(model, x)
                lpfile = "scf.lp"
            if (mod == 1):
                m.add_cycle_strong_stability(model, x)
                lpfile = "sscf.lp"
        else:
            if (rhsdelta < 0):
                error(
                    "rhs for relaxing max constraints is not defined.\n\t Probably you forgot to find max number of transplants (call model 888) or define kappa")

            coefs_tau = [-len(V) for c in C]
            tau = m.add_slack_vars(model, coefs_tau, grb.GRB.BINARY)
            m.add_relax_bound(model, coefsx, x, rhsdelta)
            if (mod == 90):
                m.add_cycle_stability(model, x, tau)
                lpfile = "scf.lp"
            if (mod == 91):
                m.add_cycle_strong_stability(model, x, tau)
                lpfile = "sscf.lp"

    ## ================================================================================================
    ## ======== edge formulations =====================================================================
    ## ================================================================================================

    if (mod == 2 or mod == 3):
        coefs = {a: 1.0 for a in arcs_dummy}
        y = m.add_y_vars(model, coefs, grb.GRB.BINARY)
        m.add_flowconservation(model, y)
        m.add_packing_edge(model, y)
        global numPaths
        numPaths = add_paths(model, y)
        if (mod == 2):
            m.add_edge_stability(model, y)
            lpfile = "sef.lp"
        if (mod == 3):
            m.add_edge_strong_stability(model, y)
            lpfile = "ssef.lp"

    ##======== edge formulation for no bound case =================
    if (mod == 20 or mod == 30 or mod == 33):
        coefs = {a: 1.0 for a in arcs_dummy}

        y = m.add_y_vars_unbounded(model, coefs, grb.GRB.BINARY)
        p = m.add_p_vars_unbounded(model, grb.GRB.INTEGER)
        m.add_flowconservation(model, y)
        m.add_packing_edge(model, y)

        if (mod == 20):
            m.add_edge_stability_unbounded(model, y, p)
            lpfile = "sefunbnd.lp"
        if (mod == 30):
            m.add_edge_strong_stability_unbounded(model, y, p)
            m.add_edge_stability_unbounded(model, y, p)
            m.add_edge_competitive_unbounded(model, y, p)
            lpfile = "ceefinf.lp"
        if (mod == 33):
            m.add_edge_strong_stability_unbounded(model, y, p)
            m.add_edge_stability_unbounded(model, y, p)
            m.add_edge_competitive_unbounded(model, y, p)
            lpfile = "ssefinf.lp"

    if (mod == 222):
        coefs = {a: 1.0 for a in arcs_dummy}
        y = m.add_y_vars_unbounded(model, coefs, grb.GRB.BINARY)
        m.add_flowconservation(model, y)
        m.add_packing_edge(model, y)
        lpfile = "ef.lp"

    # ================================================================================================
    # =============== x obj cycle-edge formulations===================================================
    # =============== 9_: x obj cycle-edge formulation ================================================
    # ================   for relaxation of stability and price of stability ===========================
    # ================================================================================================

    if (mod == 4 or mod == 5 or mod == 94 or mod == 95):
        coefsx = [len(c) for c in C]
        # coefsx = [sum(w[c[i - 1], c[i]] for i in range(len(c))) for c in C]
        x = m.add_x_vars(model, coefsx, grb.GRB.BINARY)

        coefsa = {a: 0.0 for a in arcs_dummy}
        y = m.add_y_vars(model, coefsa, grb.GRB.CONTINUOUS)

        m.add_packing(model, x)
        m.add_connection(model, x, y)
        if mod < 10:
            if (mod == 4):
                m.add_edge_stability(model, y)
                lpfile = "cef1x.lp"
            if (mod == 5):
                m.add_edge_strong_stability(model, y)
                lpfile = "scef1x.lp"
        else:

            if (rhsdelta < 0):
                error("rhs for relaxing max constraints is not defined. "
                    "Probably you forgot to find max number of transplants. call model 888")

            coefs_tau = [-len(V) for c in C]
            tau = m.add_slack_vars(model, coefs_tau, grb.GRB.BINARY)
            m.add_relax_bound(model, coefsx, x, rhsdelta)
            if (mod == 94):
                m.add_edge_stability(model, y, tau)
                lpfile = "cef1x.lp"
            if (mod == 95):
                m.add_edge_strong_stability(model, y, tau)
                lpfile = "scef1x.lp"

    # =====================================================================
    # =============== y obj cycle-edge formulations========================
    # =====================================================================

    if (mod == 6 or mod == 7):
        coefsx = [0.0 for c in C]
        x = m.add_x_vars(model, coefsx, grb.GRB.BINARY)
        coefsa = {a: 1.0 for a in arcs_dummy}
        #coefsa = {a: w[a] for a in arcs_dummy}
        y = m.add_y_vars(model, coefsa, grb.GRB.CONTINUOUS)

        m.add_packing(model, x)
        m.add_connection(model, x, y)
        if (mod == 6):
            m.add_edge_stability(model, y)
            lpfile = "cef1y.lp"
        if (mod == 7):
            m.add_edge_strong_stability(model, y)
            lpfile = "scef1y.lp"

        model.update()

    # =====================================================================
    # =============== PICEF both objectives ===============================
    # =====================================================================
    if (mod == 8 or mod == 9 or mod == 10 or mod == 11):

        calK_l, graph_l_verts, graph_l_adj_in, graph_l_adj_out = make_l_graphs_chains(chains, N, L)
        if(mod == 8 or mod == 9):
            coefsx = [len(c) for c in cycles]
            #coefsx = [sum(w[c[i - 1], c[i]] for i in range(len(c))) for c in cycles]
            coefsz = {a: 1.0 for a in arcs_dummy}
            #coefsz = {a: w[a] for a in arcs_dummy}

            coefsa = {a: 0.0 for a in arcs_dummy}


        if (mod == 10 or mod == 11):
            coefsx = [0.0 for c in cycles]
            coefsz = {a: 0.0 for a in arcs_dummy}

            coefsa = {a: 1.0 for a in arcs_dummy}
            #coefsa = {a: w[a] for a in arcs_dummy}


        x = m.add_x_vars(model, coefsx, grb.GRB.BINARY, cycles)
        z = m.add_z_vars(model, coefsz, calK_l, graph_l_adj_out, grb.GRB.BINARY)
        y = m.add_y_vars(model, coefsa, grb.GRB.CONTINUOUS)

        m.add_picef_packing(model, x, z, graph_l_adj_out, calK_l)
        m.add_nddpicef_packing(model, z, graph_l_adj_out)
        m.add_picef_positioning(model, z, graph_l_adj_in, graph_l_adj_out, calK_l, L)
        m.add_picef_connection(model, x, y, z, graph_l_adj_out, calK_l)
        if (mod == 8):
            m.add_edge_stability(model, y)
            lpfile = "picefxz.lp"
        if (mod == 9):
            m.add_edge_strong_stability(model, y)
            lpfile = "spicefxz.lp"
        if (mod == 10):
            m.add_edge_stability(model, y)
            lpfile = "picefy.lp"
        if (mod == 11):
            m.add_edge_strong_stability(model, y)
            lpfile = "spicefy.lp"
    inittime = process_time() - strt
    #model.write(lpfile)
    # input()
    if (mod > 89 and mod < 100):
        optimizemodel(mod, model, inittime, newline, endline, lpfile, x, coefsx, tau)
    else:
        optimizemodel(mod, model, inittime, newline, endline, lpfile)


def optimizemodel(mod, model, inittime, newline, endline, lpfile, vars=None, coefs=None, slack=None):
    model.setParam("TimeLimit", maxtime)

    # model.setParam("presolve", 0)
    if (mod == 4 or mod == 6):
        model.setParam("Presolve", -1)

    print("presolve", model.getParamInfo('presolve'))

    ncols = model.getAttr(grb.GRB.attr.NumVars)
    nrows = model.getAttr(grb.GRB.attr.NumConstrs)
    nnze = model.getAttr(grb.GRB.attr.NumNZs)

    # print(ncols, nrows, nnze)
    # input()

    gap = None
    gapint = None
    bestbound = None
    mipobj = None
    # =============== mip solve =======================

    strt = process_time()
    #
    # m_pres = model.presolve()
    # m_pres.write(lpfile+"pres.lp")
    # model.write(lpfile)
    model.optimize()
    opt = -1
    status = model.status
    if (status == 2):
        opt = 0
    elif (status == 9):
        opt = 1
    elif (status == 3):
        opt = 2
    else:
        print("Model status %d. Exiting" % model.status)
        exit(1)
    mipt = process_time() - strt

    if (opt == 0 or opt == 1):
        mipobj = model.objVal
        if (opt == 1):
            bestbound = model.ObjBoundC

    print("Mip solved =", mipobj, " with status", status, "in ", mipt, " seconds")

    if (mod == 888 or mod == 999 or mod == 222):
        global ObjMaxCF
        ObjMaxCF = mipobj
        if (kappa < 0 and mod == 999):
            error("No kappa defined. No relaxing stability calculations")
        if (mod == 999):
            global rhsdelta
            rhsdelta = ObjMaxCF - round(kappa * ObjMaxCF)
            writerespirce(mod, opt, None, None, inittime, mipt, newline, endline)

        if (mod == 888 or mod == 222):
            writeres(mod, None, mipobj, None, None, opt, inittime,
                     None, mipt, ncols, nrows, nnze, newline, endline)


    elif (mod < 20 or mod == 20 or mod == 30 or mod == 33):
        #######========= lp solve =======================
        #makelp(model)
        # model.write(lpfile + "lp.lp")
        model.setParam("TimeLimit", maxtime)

        strt = process_time()
        lpobj = 0
        #model.optimize()
        #lpstatus = model.status
        lpt = process_time() - strt
        lpopt = -1
        lpstatus = 2
        if (lpstatus == 2):
            lpobj = model.objVal
            print("LP solved %.2f in %.2f seconds\n\n" % (lpobj, lpt,))
            lpopt = 0
        elif (lpstatus == 9):
            print("LP time limit")
            lpopt = 1
        elif (lpstatus == 3):
            lpopt = 2
            print("LP infeasible")
        else:
            print("unknown lpstatus exiting")
            exit(1)

        if (lpopt == 2 and opt == 0):
            print("!!Exiting. LP is infeas and MIP is feas")
            exit(1)
        if ((opt == 0 or opt == 1) and lpopt == 0):
            gap = 100.0 * (lpobj - mipobj) / mipobj
        if (opt == 1):
            gapint = 100.0 * (bestbound - mipobj) / mipobj

        # print("gap", gap)
        # print("gapint", gapint)
        # print("lp", lpobj,"mip", mipobj,"BUB", bestbound)
        # input()

        # ========= write results in files ====================
        writeres(mod, lpobj, mipobj, gap, lpopt, opt, inittime,
                 lpt, mipt, ncols, nrows, nnze, newline, endline, gapint)

    elif (mod > 89 and mod < 100 or mod == 888):
        if (vars == None or coefs == None or slack == None):
            error(
                "there is not vars, coefs or slack send. they are necessary to output ntrans and number ob Blocking cycles")
        mipobj = -1
        modTrans = -1
        modnB = -1

        if (mipobj != grb.GRB.INFINITY):
            modTrans = 0.0
            modnB = 0.0
            for i in range(len(vars)):
                if vars[i].X > 0.5:
                    modTrans += coefs[i]
            # print(coefs[i])
            for i in range(len(slack)):
                if slack[i].X > 0.5:
                    modnB += 1
        else:
            error("Mipojb is infinity. is it a mistake??")
        print("model Transplants", modTrans)
        print("model Blocking cycles", modnB)

        # ========= write results in files ====================
        writerespirce(mod, opt, modnB, modTrans, inittime, mipt, newline, endline)


def writeres(mod, lpopt, mipopt, gap, lpst, opt, tinit, tlp, tmip, ncols, nrows, nnze, newline, endline, gapint=None):
    ftex = open(fileoutres, 'a')
    if (newline == True):
        ftex.write(instname + "\t" + str(len(P)) + "\t" +
                   str(len(N)) + "\t" + str(len(arcs)) + "\t" + "%.2f" % preptime + "\t"
                   + str(numCycles) + "\t" + str(numChains))

    if (mod == 888 or mod == 222):
        return
    ftex.write("\t" + str(mod) + "\t" + str(lpopt) + "\t" + str(mipopt))

    if (gap != None):
        ftex.write("\t" + "%.2f" % gap + "\t" + str(opt))
    else:
        ftex.write("\t" + str(gap) + "\t" + str(opt))

    if (lpst == 0):
        ftex.write("\t" + "%.2f" % tinit + "\t" + "%.2f" % tlp)
    else:
        ftex.write("\t" + "%.2f" % tinit + "\t" + "tllp")

    if (opt == 0):
        ftex.write("\t" + "%.2f" % tmip + "\t-")
    elif (opt == 1):
        ftex.write("\t" + "tl" + "\t%.2f" % gapint)
    elif (opt == 2):
        ftex.write("\t" + "%.2f" % tmip + "\tinf")
    else:
        print("Wrong opt in writeres. exiting")
        exit(1)

    ftex.write("\t%d\t%d\t%d" % (ncols, nrows, nnze,))

    if (endline):
        # print("numPaths", numPaths)
        ftex.write("\t" + str(numPaths))
        if (ObjMaxCF < 0):
            ftex.write("\n")
        else:
            if (opt != 2):
                price = 1.0 * (ObjMaxCF - mipopt) / ObjMaxCF * 100.0
                ftex.write("\t" + "%.2f" % price + "\n")
            else:
                ftex.write("\t" + "-" + "\n")

    ftex.close()


def writerespirce(mod, optstatus, nB, ntrans, initt, mipt, newline, endline):
    ftex = open(fileoutres, 'a')
    if (newline == True):
        ftex.write(instname + "\t" + str(len(P)) + "\t" +
                   str(len(N)) + "\t" + str(len(arcs)) +
                   "\t" + str(K) + "\t" + str(L) + "\t" + "%.2f" % preptime + "\t"
                   + str(numCycles) + "\t" + str(numChains) + "\t" + str(kappa))
    if (mod == 999):
        ftex.write("\t" + str(mod) + "\t" +
                   str(ObjMaxCF) + "\t" + str(rhsdelta))
    else:
        if (ntrans >= 0):
            ftex.write("\t" + str(mod) + "\t" + str(optstatus) + "\t" + str(ntrans) + "\t" + str(nB)
                       + "\t" + "%.2f" % mipt)
        else:
            ftex.write("\t" + str(mod) + "\t" + str(optstatus) + "\t-\t-"
                       + "\t" + "%.2f" % mipt)

    if (endline):
        # print("numPaths", numPaths)
        ftex.write("\n")
    ftex.close()


if __name__ == '__main__':
    import sys
    import os
    try:
        K = int(sys.argv[1])
        L = int(sys.argv[2])
        fileoutres = sys.argv[3]
        stability_type = int(sys.argv[4])
        preferences = sys.argv[5]
        if (len(sys.argv) < 7):
            kappa = -1

        else:
            kappa = float(sys.argv[6])

    except:
        K = 3
        L = 3
        fileoutres = "test.out"
        stability_type = 0
        preferences = "S"
        #kappa = -1.0
        kappa = 0.1

    print("======================================================")
    print("\t Run settings:")
    print("\t\t K=", K, " L=", L)
    print("\t\toutput file: \t", fileoutres)
    print("\t\tstability:\t ", stability_type, "\t(0-stable, 1-strongly stable)")
    print("\t\tpreferences \t", preferences)
    print("======================================================")

    Sizes = [20]
    instancesset = range(5)
    modecodes = []
    ### =====  run models ===========================
    if (stability_type == 0 ):
        modcodes = [888, 6, 8, 10 ]
    elif (stability_type == 1):
        modcodes = [888, 7, 9, 11]
    else:
        print("stability_type parameter is not correct: 0 for stability, 1 for strong stability")
        exit()
    # ====================================================================================

    # ==== to run models for analisis of the trade-off between objectives ================
    # if (stability_type == 0 ):
    #     modcodes = [999, 94]
    # elif (stability_type == 1):
    #     modcodes = [999, 95]
    # else:
    #     print("stability_type parameter is not correct: 0 for stability, 1 for strong stability")
    #     exit()
    # ====================================================================================


    if (modcodes[1] < 90):
        make_hedears_forms(fileoutres, len(modcodes) - 1)
    else:
        make_headers_price(fileoutres, len(modcodes))

    for size in Sizes:
        # for inst in range(numinst):
        for inst in instancesset:
            if (preferences == "S"):
                filename = "instances_all/%d_%d_wr.input" % (size, inst + 1,)
            elif (preferences == "N"):
                filename = "instances_nonstrict/%d_%d_wr.input" % (size, inst + 1,)
            else:
                print("Error: Preferences parameter not defined: S or N")
                exit(1)
            filenameranks = filename + ".donors"

            print("\n============================\n", filename, filenameranks)

            sstr1 = filename.replace("instances_all/", "")
            sstr = sstr1.replace("instances_nonstrict/", "n")
            instname = sstr.replace("_wr.input", "")
            starttime = process_time()

            P, N, arcs, w, r = read_data(filename)
            donor_ranks = read_donors_ranks(filenameranks, len(P))

            V = P | N
            adj_in, adj_out = makeadj(arcs, V)
            # print(adj_out)

            cycles = get_all_cycles(P, adj_out, K)
            chains = get_all_chains(N, adj_out, L)


            numCycles = len(cycles)
            numChains = len(chains)

            print("Cycles and chains enumerated")
            C = cycles.copy()
            for s in chains:
                C.append(s)

            # indc = 0
            # for c in C:
            # 	print(indc, ":", c)
            # 	indc += 1

            arcs_dummy, adj_in_dummy, adj_out_dummy, dummy = add_dummy_arcs(P, N, arcs, adj_in, adj_out, r, w,
                                                                            donor_ranks)

            # for j in adj_in_dummy:
            # 	print(j,":")
            # 	for i in adj_in_dummy[j]:
            # 		print("\t", i, "(",r[i,j], "), ")

            print("Dummy made")

       #     make_l_graphs(P, N, adj_out_dummy, L)
            print("graphs from chains")
            make_l_graphs_chains(chains, N, L)



            vinc, vincycles, ainc, aincycles = makeVertsArcsCycles(C, cycles, V, arcs, arcs_dummy)
            print("Cycles/chais for each vertex and arc are listed")


            preptime = process_time() - starttime

            m = modelling()
            m.V = V
            m.P = P
            m.N = N
            m.C = C
            m.aincyc = aincycles
            m.vincyc = vincycles
            m.ainc = ainc
            m.vinc = vinc
            m.arcs_dummy = arcs_dummy
            m.r = r
            m.arcs = arcs
            m.adj_out = adj_out
            m.adj_in = adj_in
            m.adj_out_dummy = adj_out_dummy
            m.adj_in_dummy = adj_in_dummy
            m.adj_in = adj_in

            # modcodes = [0,4,6,12,14]

            for mm in modcodes:
                nl = False
                el = False
                if (modcodes.index(mm) == 0):
                    nl = True
                if (modcodes.index(mm) == len(modcodes) - 1):
                    el = True
                model(mm, nl, el)
                print("model %d is done \n================================\n\n" % (mm,))
