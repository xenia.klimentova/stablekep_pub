import gurobipy as grb
import copy


def error(s):
    print("!!!!!!!!!!Error.\n\t", s)
    exit(1)


def makeadj(arcs, V):
    adj_out = {}
    adj_in = {}
    for v in V:
        adj_out[v] = set()
        adj_in[v] = set()
    for a in arcs:
        i = a[0]
        j = a[1]
        adj_out[i].add(j)
        adj_in[j].add(i)
    return adj_in, adj_out


def normalize_chain(c, a):
    ia = c.index(a)
    clist = list(c)
    c_new = clist[ia:]
    for v in clist[:ia]:
        c_new.append(v)
    c_new = tuple(c_new)
    return c_new


def add_dummy_arcs(P, N, arcs, adj_in, adj_out, r, w, r_donors):
    arcs_dummy = arcs.copy()
    adj_in_dummy = copy.deepcopy(adj_in)
    adj_out_dummy = copy.deepcopy(adj_out)
    dummy = set()
    # print(r_donors)
    for p in P:
        for a in N:
            arcs_dummy.add((p, a))
            adj_in_dummy[a].add(p)
            adj_out_dummy[p].add(a)
            r[p, a] = r_donors[p]
            w[p, a] = 1.0 * r_donors[p] / (len(P) + 1)
            dummy.add((p, a))

    return arcs_dummy, adj_in_dummy, adj_out_dummy, dummy


def makeVertsArcsCycles(C, cycles, V, arcs, arcs_dummy):
    ainc = {}
    aincycles = {}
    vinc = {}
    vincycles = {}

    for a in arcs_dummy:
        ainc[a] = []
    for a in arcs:
        aincycles[a] = []
    for v in V:
        vinc[v] = []
        vincycles[v] = []

    for c in C:
        ic = C.index(c)
        for i in range(len(c)):
            a = (c[i - 1], c[i])
            ainc[a].append(ic)
            vinc[c[i]].append(ic)
    for c in cycles:
        ic = cycles.index(c)
        for i in range(len(c)):
            a = (c[i - 1], c[i])
            aincycles[a].append(ic)
            vincycles[c[i]].append(ic)

    # for a in arcs:
    #     print(a, ": ", aincycles[a])
    # print()
    # for a in arcs_dummy:
    #     print(a, ": ", ainc[a])
    # for v in V:
    #     print(v, ": ", vinc[v])
    #     print(v, ": ", vincycles[v])

    return vinc, vincycles, ainc, aincycles


def makemip(model):
    vars = model.getVars()
    for v in vars:
        v.vtype = grb.GRB.BINARY
    model.update()


def makelp(model):
    vars = model.getVars()
    for v in vars:
        v.vtype = grb.GRB.CONTINUOUS
    model.update()


def writesol(model, x, y, arcs_dummy, C):
    if (x != None):
        print("Cycles:")

        for i in range(len(C)):
            if (x[i].X > 0.5):
                print(i, " ")
    if (y != None):
        print("arcs: ")
        for a in arcs_dummy:
            if (y[a].X > 0.5):
                print(a, " ")


def make_hedears_forms(fileres, nforms):
    fres = open(fileres, "a")
    fres.write("name\tP\tN\tA\tprepT\tnCyc\tnChain")
    for i in range(nforms):
        fres.write("\tmod\tlpopt\tmipopt\tgap\topt\ttinit\ttlp\ttmip\tgapint\tncols\tnrows\tnnze")
    fres.write("\tnPaths\tstabPrice\n")
    fres.close()


def make_cp_hedears(fileres, nforms):
    fres = open(fileres, "a")
    fres.write("name\tP\tN\tA\ttprepT")
    for i in range(nforms):
        fres.write("\tmod\tmipopt\titer\ttinit\ttmip\ttviol\ttcp")
    fres.write("\n")
    fres.close()


def make_hedears_relaxed(fileres, nforms):
    fres = open(fileres, "a")
    fres.write("name\tV\tP\tA\tprepT\tnCyc\tnChain")
    for i in range(nforms):
        fres.write("\tmod\t1stop\t2ndopt\tn3b\tn2b\tt1st\tt2nd\tttotal")
    fres.write("\n")
    fres.close()


def make_headers_price(fileres, nforms):
    fres = open(fileres, "a")
    fres.write("name\tP\tN\tA\tK\tL\tprepT\tnCyc\tnChain\tkappa")
    fres.write("\tmod\tObjMaxCF\trhsDelta")

    for i in range(nforms):
        fres.write("\tmod\tstatus\tNtrans\tnBlock\ttmip")
    fres.write("\n")
    fres.close()


def floyd_warshall_algorithm(verts, arcs, ndds, l):

    n = len(verts)
    dist = [[n*n for i in range(n)] for j in range(n)]

    for i in range(n):
        dist[i][i] = 0

    for a in arcs:
        if a[0] in ndds and a[0] != l:
            continue
        if a[1] in ndds and a[1] != l:
            continue

        ii = verts.index(a[0])
        jj = verts.index(a[1])
        dist[ii][jj] = 1


    for k in range(n):
        for i in range(n):
            for j in range(n):
                if dist[i][j] > dist[i][k] + dist[k][j]:
                    dist[i][j] = dist[i][k] + dist[k][j]

    return dist
def dijkstra_algorithm(l, verts, adj_out, L):
    n = len(verts)
    dist = [n*n for i in range(n)]
    dist[verts.index(l)] = 0
    visited = [False for i in range(n)]
    while 1:
        min = n*n
        minvert = -1
        # choose vertex with min distance
        for i in range(n):
            if dist[i] < min and not visited[i]:
                min = dist[i]
                minvert = i
        if minvert < 0:
            break
        if min >= L-1:
            break
        visited[minvert] = True
        for j in adj_out[verts[minvert]]:
            jj = verts.index(j)
            if visited[jj]:
                continue
            if dist[jj] > dist[minvert] + 1:
                dist[jj] = dist[minvert] + 1
    print("l:", l, "  ", dist)

    return dist

def l_graph(l, verts, adj_out, L):
    l_verts = []
    l_adj_out = {}
    l_adj_in = {}
    dist = dijkstra_algorithm(l, verts, adj_out, L)
    for v in verts:
        if v == l:
            continue
        vv = verts.index(v)
        ll = verts.index(l)
        if dist[vv] + 1 <= L:
            if len(l_verts) == 0:
                l_verts.append(l)
            l_verts.append(v)
    for v in l_verts:
        l_adj_out[v] = set()
        l_adj_in[v] = set()

    for i in adj_out:
        for j in adj_out[i]:
            ii = verts.index(i)
            ll = verts.index(l)
            jj = verts.index(j)
            if dist[ii] + 2 <= L:
                l_adj_out[i].add(j)
                l_adj_in[j].add(i)
    return l_verts, l_adj_in, l_adj_out

def make_l_graphs(P, ndds, adj_out, L):
    for l in ndds:
        verts = list(P)
        verts.append(l)
        verts.sort()
        lout = {v: set() for v in verts}
        for i in adj_out:
            if i in ndds and i != l:
                continue
            for j in adj_out[i]:
                if j in ndds and j != l:
                    continue
                lout[i].add(j)
        print("data for dijkstra")
        print(verts)
        print(lout)

        l_verts, l_adj_in, l_adj_out = l_graph(l, verts, lout, L)
        print("subgraph ", l)
        print(l_verts)
        print(l_adj_out)
        print(l_adj_in)

def make_l_graphs_chains(chains, ndds, L):
    graph_l_verts = {}
    graph_l_adj_out = {}
    graph_l_adj_in = {}
    calK_l = {}
    for c in chains:
        if c[0] not in ndds:
            print("something went wrong, NDD is not the first in the tuple of chain")
            exit()
        l = c[0]
        if l not in graph_l_verts:
            graph_l_verts[l] = set()
            graph_l_adj_out[l] = {}
            graph_l_adj_in[l] = {}
            calK_l[l] = {}


        for i in range(len(c)):
            graph_l_verts[l].add(c[i])
            a = (c[i-1], c[i])
            if a[0] == l:
                calK_l[l][a] = [1]
            elif a[1] == l:
                calK_l[l][a] = list(range(2, L+1))
            else:
                calK_l[l][a] = list(range(2, L))
            if c[i-1] not in graph_l_adj_out[l]:
                graph_l_adj_out[l][c[i-1]] = set()
            graph_l_adj_out[l][c[i-1]].add(c[i])

            if c[i] not in graph_l_adj_in[l]:
                graph_l_adj_in[l][c[i]] = set()
            graph_l_adj_in[l][c[i]].add(c[i-1])

    # print(graph_l_verts)
    # print(graph_l_adj_out)
    # print(graph_l_adj_in)
    # print(calK_l)
    return calK_l, graph_l_verts, graph_l_adj_in, graph_l_adj_out
